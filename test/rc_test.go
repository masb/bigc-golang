package main

import "testing"

import "codeberg.org/masb/bigc-golang.git/src"

func TestRCParse(t *testing.T) {
    var m map[string]interface{}
    err := bigc.RCParse("hello.rc", &m)
    if err != nil {
        t.Fatal(err)
    }
    if len(m) != 1 {
        t.Fatalf("expected 1 item, got %d", len(m))
    }
    if v, ok := m["hello"]; !ok {
        t.Fatalf("expected hello item")
    } else {
        if vs, ok := v.(string); !ok {
            t.Fatalf("expected 'world', got %v", v)
        } else if vs != "world" {
            t.Fatalf("expected 'world', got %s", vs)
        }
    }
}
