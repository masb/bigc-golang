#include <bigc/data/rc.h>

#include "_cgo_export.h"

void rc_parse_file(char const *path, GoMap *gm, GoInterface *gerr)
{
    umap m;
    io_param p = {};
    p.ipath = path, p.omap = &m;
    errc err = rc_parse(IO_FILE_UMAP, p);
    if (err.code != ERR_NONE) {
        go_err_str(gerr, (char *)err_msg(err));
        err_release(err);
        return;
    }
    *gerr = (GoInterface) { 0 };
    go_map_make(gm, umap_len(&m));
    for (rc_map *i = rc_begin(&m); i != NULL; i = rc_next(i)) {
        go_map_set_str(gm, (char *)rc_key(i), i->value);
    }
    rc_release(&m);
}
