package bigc

/*
    void free(void *);
    void rc_parse_file(char const *, void *, void *);
*/
import "C"

import "unsafe"

func RCParse(path string, m *map[string]interface{}) error {
    var err error
    var cpath = C.CString(path)
    C.rc_parse_file(cpath, unsafe.Pointer(m), unsafe.Pointer(&err))
    C.free(unsafe.Pointer(cpath))
    return err
}
